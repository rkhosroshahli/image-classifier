
let mobileNet;
const webcamElement = document.getElementById('webcam');
async function app() {
    mobileNet = await mobilenet.load();
    console.log('Successfully loaded model');
    const img_el = document.getElementById("img");
    const result = await mobileNet.classify(img_el);
    document.getElementById('result').innerHTML = "prediction: " + result[0].className + " <br> probability: " + result[0].probability;
    /*     console.log(navigator.mediaDevices)
        if (navigator.mediaDevices.getUserMedia) {
            navigator.mediaDevices.getUserMedia({ video: true })
                .then(stream => {
                    webcamElement.srcObject = stream;
                })
                .catch(err =>
                    console.log(err)
                );
    
        } */
    /*     const webcam = await tf.data.webcam(webcamElement);
        while (true) {
            const img = await webcam.capture();
            const result = await mobileNet.classify(img);
            console.log(result);
            document.getElementById('result').innerHTML = "prediction: " + result[0].className + " probability: " + result[0].probability;
            img.dispose();
            await tf.nextFrame();
        } */


}

// app();

function btnHandler(){
    var text_in = document.getElementById("url_in");
    if (text_in.value.length === 0) {
        var sel_in = document.getElementById("sel_in");
        var link = sel_in.options[sel_in.selectedIndex].value;
        document.getElementById("img").src = link;
    }else{
        var link = text_in.value;
        document.getElementById("img").src = link;
    }
    app();
}